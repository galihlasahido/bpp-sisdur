package co.id.bankdki.gga.sisdur.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by bankdki on 6/14/15.
 */
@Controller
public class Dashboard {

    @RequestMapping(value = {"/","/home"}, method = RequestMethod.GET)
    public String getHome() {
        return "";
    }

}
