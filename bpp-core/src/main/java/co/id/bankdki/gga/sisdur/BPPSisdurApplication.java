package co.id.bankdki.gga.sisdur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by bankdki on 6/14/15.
 */
@SpringBootApplication
@PropertySource("file:./config/application.properties")
public class BPPSisdurApplication {
    public static void main(String[] args) {
        SpringApplication.run(BPPSisdurApplication.class, args);
    }

}
